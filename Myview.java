/**

      * 返回一个扇形的剪裁区

      * @param canvas  //画笔

      * @param center_X  //圆心X坐标

      * @param center_Y  //圆心Y坐标

      * @param r         //半径

      * @param startAngle  //起始角度

      * @param sweepAngle  //终点角度

      * 

      */ 

    private void getSectorClip(Canvas canvas,float center_X,float center_Y,float r,float startAngle,float sweepAngle) 

    { 

        Path path = new Path(); 

         //下面是获得一个三角形的剪裁区 

        path.moveTo(center_X, center_Y);  //圆心 

        path.lineTo((float)(center_X+r*Math.cos(startAngle* Math.PI / 180)),   //起始点角度在圆上对应的横坐标 

                (float)(center_Y+r*Math.sin(startAngle* Math.PI / 180)));    //起始点角度在圆上对应的纵坐标 

        path.lineTo((float)(center_X+r*Math.cos(sweepAngle* Math.PI / 180)),  //终点角度在圆上对应的横坐标 

                (float)(center_Y+r*Math.sin(sweepAngle* Math.PI / 180)));   //终点点角度在圆上对应的纵坐标 

        path.close(); 

//      //设置一个正方形，内切圆 

        RectF rectF = new RectF(center_X-r,center_Y-r,center_X+r,center_Y+r); 

        //下面是获得弧形剪裁区的方法     

        path.addArc(rectF, startAngle, sweepAngle - startAngle);   

        canvas.clipPath(path); 

    } 
